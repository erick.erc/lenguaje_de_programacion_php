<?php

    //Realizar una expresión regular que detecte emails correctos.
    function validarEmail ($string){
        $valor = null;
        return preg_match('/^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/', $string, $valor);
    }

    //Realizar una expresion regular que detecte Curps Correctos
    //ABCD123456EFGHIJ78.
    function validarCurp ($string){
        $valor = null;
        return preg_match('/^([A-Z]{4}+)([0-9]{6}+)([A-Z]{6}+)([0-9]{2})$/', $string, $valor);
    }

    //Realizar una expresion regular que detecte palabras de longitud mayor a 50
    //formadas solo por letras.
    function validarCadena50($string){
        $valor = null;
        return preg_match('/^[A-Za-z]{50,}$/', $string, $valor);
    }

    //Crea una funcion para escapar los simbolos especiales.
    function escapar($string) {
        $string = str_replace(' ', '-', $string); 
        return preg_replace('/[^A-Za-z0-9\-]/', '', $string);
    }

    //Crear una expresion regular para detectar números decimales.
    function validarDecimal($string){
        $valor = null;
        return preg_match('/^[0-9]+([.][0-9]+)?$/', $string, $valor);
    }

?>