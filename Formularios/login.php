<?php
    session_start();
    if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true){
        header("location: info.php");
        exit;
    }
    $_SESSION['Alumno']=array(
        array(
            'num_cta' => '1',
            'nombre' => 'Admin',
            'primer_apellido' => '',
            'segundo_apellido' => '',
            'contrasena' => 'adminpass123',
            'genero' => '0',
            'fecha_nac' => '25/01/1990'
        ),
    );

    if($_SERVER["REQUEST_METHOD"] == "POST"){
        $username=$_POST["username"];
        $pasword=$_POST["password"];
        $max=sizeof($_SESSION['Alumno']);
        for($i=0; $i<$max; $i++) { 
            while (list ($key, $val) = each ($_SESSION['Alumno'][$i])){ 
                if($key =='num_cta' && $val === $username){
                    $_SESSION["username"] = $username;
                }
                if($key == 'contrasena' && $val === $pasword){
                    $_SESSION["password"] = $pasword;
                }
            }
        }
        if(isset($_SESSION["username"]) && isset($_SESSION["password"]) && $_SESSION['username'] == $username && $_SESSION['password'] == $pasword){
            $_SESSION["loggedin"] = true;
            header("Location: info.php");
        }else{
            echo('<script type="text/JavaScript">  
                window.alert("Lo sentimos, alguno de los valores son incorrectos, favor de volver a ingresar los datos solicitados");
                </script>'); 
        }
    }

?>

<!DOCTYPE html>
<html>
    
    <head>
        <meta charset="UTF-8">
        <title>Login</title>
        <link rel="stylesheet" type="text/css" href="styles_index.css">
        <link rel="stylesheet" type="text/css" href="styles_input.css">
    </head>

    <body>
        <div class="container">
            <div class="logo">Login</div>
            <div class="logo-item">
    
                <form action="" method="post" class="form form-login"> 
    
                    <div class="form-field">
                        <label class="user" for="login-username"><span class="hidden">Username</span></label>
                        <input type="text" name="username" class="form-input" placeholder="Numero de cuenta" required>
                    </div>
        
                    <div class="form-field">
                        <label class="lock"><span class="hidden">Password</span></label>
                        <input type="password" name="password" class="form-input" placeholder="Contraseña" required>
                    </div>
        
                    <div class="form-field">
                        <input type="submit" value="Entrar">
                    </div>
    
                </form>
                
            </div>
        </div>
    </body>

</html>