<?php
    session_start();
    unset($_SESSION['Alumno']);
    
    if(session_destroy()) {
       header("Location: login.php");
    }
?>