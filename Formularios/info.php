<?php
    session_start();
    if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
        header("location: login.php");
        exit;
    }
?>

<!DOCTYPE htmal>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Home</title>
        <link rel="stylesheet" type="text/css" href="styles_formulario.css">
        <link rel="stylesheet" type="text/css" href="styles_input.css">
        <link rel="stylesheet" type="text/css" href="styles_info.css">

        <nav>
            <ul>
                <li><a href="info.php">Home</a></li>
                <li><a href="formulario.php">Registrar Alumnos</a></li>
                <li><a href="logout.php">Cerrar Sesión</a></li>
            </ul>
        </nav>
    </head>

    <body>
        <div class="containerHome">
            <div class="logo">Usuario Autenticado</div>
            <div class="logo">Informacion</div>
            <div class="logo-item">
                <?php
                    $table = '<ol id="lista">';
                    foreach ($_SESSION['Alumno'][0] as $key => $valor) {
                        if(isset($valor) && empty($valor) != true){
                            $table.= '<li>'.$valor.'</li>';
                        }
                    }
                    $table.='</ol>';
                    echo $table;
                ?>
                
            </div>
            <div class="logo-item"></div>         

        </div>

        <div class="containerHome">
            <div class="logo">Datos Guardados</div>
            <div class="logo-item">
                <?php
                    $table = '<table>';
                    $table.='   <thead>
                                    <tr>
                                        <th scope = "row">Número de Cuenta</th>
                                        <th scope= "row">Nombre</th>
                                        <th scope= "row">Primer apellido</th>
                                        <th scope = "row">Segundo apellido</th>
                                        <th scope = "row">Contraseña</th>
                                        <th scope = "row">Género</th>
                                        <th scope = "row">Fecha de Nacimiento</th>
                                    </tr>
                                </thead>';
                    foreach($_SESSION['Alumno'] as $key){
                        $table.='<tr>';
                        foreach($key as $valor){
                            if(empty($valor)){
                                $table.= '<td>'.'&nbsp;'.'</td>';
                            }else{
                                $table.= '<td>'.$valor.'</td>';
                            }
                        }
                        $table.='</tr>';
                    }
                    $table.='</table>';
                    echo $table;
                ?>
            </div>
            <div class="logo-item"></div>      

        </div>
    </body>

</html>