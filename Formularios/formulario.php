<?php   
    session_start();
    if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
        header("Location: login.php");
        exit;
    }

    if($_SERVER["REQUEST_METHOD"] == "POST"){
        $formulario=array(
            'num_cta' => $_POST['num_cta'],
            'nombre' => $_POST['nombre'],
            'primer_apellido' => $_POST['primer_apellido'],
            'segundo_apellido' => $_POST['segundo_apellido'],
            'contrasena' => $_POST['contrasena'],
            'fecha_nac' => $_POST['contraseña'],
            'genero' => $_POST['genero'],
        );
        array_push($_SESSION['Alumno'], $formulario);
        header("Location: info.php");
    }

?>

<!DOCTYPE html>
<html>

    <head>
        <meta charset="UTF-8">
        <title>Formulario</title>
        <link rel="stylesheet" type="text/css" href="styles_formulario.css">
        <link rel="stylesheet" type="text/css" href="styles_input.css">

        <nav>
            <ul>
                <li><a href="info.php">Home</a></li>
                <li><a href="formulario.php">Registrar Alumnos</a></li>
                <li><a href="logout.php">Cerrar Sesión</a></li>
            </ul>
        </nav>
    </head>

    <body>
        <div class="container">
            <div class="logo">Usuario Autenticado</div>
            <div class="logo-item">
                <form action="" method="post" class="form form-login">

                    <div class="form-field">
                        <label><span>Número de Cuenta</span></label>
                        <input type="text" name="num_cta" placeholder="Número de cuenta" required>
                    </div>
    
                    <div class="form-field">
                        <label><span>Nonmre</span></label>
                        <input type="text" name="nombre" placeholder="Nombre" required>
                    </div>
    
                    <div class="form-field">
                        <label><span>Primer Apellido</span></label>
                        <input type="text" name="primer_apellido" placeholder="Primer Apellido" required>
                    </div>
    
                    <div class="form-field">
                        <label><span>Segundo Apellido</span></label>
                        <input type="text" name="segundo_apellido" placeholder="Segundo Apellido">
                    </div>
    
                    <div class="form-field">
                        <label><span>Género</span></label><br>
                        <label><input type="radio" id="hombre" name="genero" value="H" required>Hombre</label>
                        <label><input type="radio" id="mujer" name="genero" value="M">Mujer</label>
                        <label><input type="radio" id="otro" name="genero" value="O">Otro</label>
                    </div>
    
                    <div class="form-field">
                        <label><span>Fecha de Nacimiento</span></label>
                        <input type="date" name="contraseña">
                    </div>
    
                    <div class="form-field">
                        <label><span>Contraseña</span></label>
                        <input type="password" name="contrasena" placeholder="Contraseña" required>
                    </div>
    
                    <div class="form-field">
                        <a href="formulario.php"><input type="submit" value="Registrar"></a>
                    </div>
    
                </form>
            </div>
        </div>
    </body>


</html>